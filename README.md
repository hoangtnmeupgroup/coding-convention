# Coding Convention 
# Start
## Getting started
Coding conventions là tập hợp những nguyên tắc chung khi lập trình nhằm làm cho code dễ đọc, dễ hiểu, do đó dễ quản lý, bảo trì hơn: 
-	80% khối lượng dòng code của một phần mềm dự án được dùng để bảo trì
-	Khó có thể tìm thấy phần mềm nào mà cả vòng đời của nó vẫn giữ nguyên nguyên bản gốc. 
-	Quy chuẩn code cải thiện khả năng mạch lạc, logic và dễ hiểu của phần mềm. Các lập trình viên có thể hiểu những dòng code mới một cách nhanh chóng. 
-	Nếu bạn “gửi” mã code của bạn như một sản phẩm, bạn cần chắc chắn nó cần được đóng gói cẩn thận và thật “clear”.

Ok, Dưới đây là coding conventions:
## File Names
-	Tên file nên được đặt mô tả rõ mục đích, nội dung của file. Tên file không nên đặt quá dài, hãy giữ tên file ngắn và có ý nghĩa. 
-	Không nên sửa dụng “dấu cách”. Thay vì đó có thể sử dụng:
  * Gạch ngang: file-name.ts (được ưa dùng)
  * Gạch chân dưới: file_name.ts
  * Camel: FileName.ts
-	Một số ký tự không được phép dùng: ~ ! @ # $ % ^ & * ( ) ` ; < > ? , [ ] { } ‘ “

## File Organization
-	Nội dung của 1 file hơn 2000 dòng sẽ “cồng kềnh” và nên tránh.

## Indentation
-	4 khoảng trắng nên được sử dụng làm đơn vị thụt lề. Các tab phải được đặt chính xác cứ sau 8 khoảng trắng.
-	Tránh một dòng dài hơn 80 ký tự.
-	Khi một câu lệnh không đủ ở một dòng, hãy xuống dòng (break) theo những nguyên tắc chung:
  * Break sau dấu phẩy “,”
  * Break trước một toán tử
  * Ưu tiên break câu lệnh có “level” cao hơn
  * Break khi bạn bắt đầu viết câu lệnh cùng level với dòng trước đó
  * Nếu các quy tắc trên dẫn đến mã khó hiểu hoặc mã bị đè lên lề phải. Thay vào đó chỉ cần thụt lề 8 “spaces”

  # Example
  someMethod(longExpression1, longExpression2, longExpression3,
  longExpression4, longExpression5);
  var = someMethod1(longExpression1,
          someMethod2(longExpression2,
                  longExpression3));


  longName1 = longName2 * (longName3 + longName4 - longName5)+ 4 * longname6; // PREFER
  longName1 = longName2 * (longName3 + longName4
  - longName5) + 4 * longname6; // AVOID
                  longExpression3));

Line wrapping for if statements should generally use the 8-space rule, since conventional (4-spaec) indentation makes seeing the body difficult.

  //DON'T USE THIS INDENTATIONif ((condition1 && condition2)
  || (condition3 && condition4)
  ||!(condition5 && condition6)) { //BAD WRAPS
  doSomethingAboutIt();            //MAKE THIS LINE EASY TO MISS
  }
  //USE THIS INDENTATION INSTEAD
  if ((condition1 && condition2)
  || (condition3 && condition4)
  ||!(condition5 && condition6)) {
  doSomethingAboutIt();
  }
  //OR USE THIS
  if ((condition1 && condition2) || (condition3 && condition4)
  ||!(condition5 && condition6)) {
  doSomethingAboutIt();

Here are three acceptable ways to format ternary expressions. 

  alpha = (aLongBooleanExpression) ? beta : gamma;  alpha = (aLongBooleanExpression) ? beta
  : gamma;
  alpha = (aLongBooleanExpression)
  ? beta
  : gamma;

## Comments
Comments cung cấp cái nhìn tổng quan về đoạn code và cung cấp thông tin bổ sung không có sẵn trong chính mã code. Comments chỉ nên chứa thông tin liên quan đến việc đọc hiểu chương trình. 
Rất dễ để những bình luận dư thừa, hãy tránh bất kỳ nhận xét nào có thể bị lỗi thời. 
Lưu ý: Tần suất comments đôi khi phản ánh chất lượng code kém. Khi bạn cảm thấy bắt buộc phải thêm nhận xét, hãy cân nhắc comments để làm rõ ràng hơn.
Có 4 kiểu comments: 
- Block 
- Single-line
- Trailing
- End-of-line

Nào giờ, ta cùng xem 4 kiểu comments đó như thế nào nhé.
- Thứ nhất, Block comments được sử dụng để cung cấp mô tả files, phương thức và cấu trúc dữ liệu. Block comments có thể được bắt đầu ở mỗi dòng trước mỗi hàm. Chúng cũng có thể được sử dụng ở những chỗ khác ví dụ ở trong hàm. Block comments nên nằm trong hàm hoặc phương thức và cùng lề với đoạn mã code mà bạn muốn mô tả.

/*
 * Here is a block comment.
*/

/*-* Here is a block comment with some very special
* formatting that I want indent(1) to ignore.
*
*    one
*        two
*            three
*/


- Thứ hai, Single-Line comments có thể xuất hiện ở trên dòng với đoạn code mà bạn muốn mô tả. Nếu comment không thể viết trong 1 dòng, nó nên theo block-comment format. 

if (condition) {/* Handle the condition. */
...
}

- Thứ ba, Trailing comments có thể xuất hiện ở cùng dòng với đoạn code mà bạn muốn mô tả nhưng nên dịch đủ xa để cách câu lệnh. Nếu nhiều hơn 1 comment ngắn xuất hiện, chúng nên cùng cách “tab”.

if (a == 2) {return TRUE;            /* special case */
} else {
return isPrime(a);      /* works only for odd a */
}

- Cuối cùng là End-Of-Line comments. Ký tự “//” có thể comment ngay sau một dòng code. Bạn không nên sử dụng trên nhiều dòng liên tiếp để mô tả đoạn code. 

if (foo > 1) {// Do a double-flip.
...
}
else {
return false;          // Explain why here.
}
//if (bar > 1) {
//
//    // Do a triple-flip.
//    ...
//}
//else {
//    return false;
//}

## Declarations
-	Number Per Line: Mỗi một dòng khởi tạo nên gồm end-of-line comments mô tả biến được dùng để làm gì.
  int level; // indentation level
  int size;  // size of table

  * Không nên dùng:
  int level, size;
  * Hoặc không khởi tạo biến với các kiểu khác nhau ở cùng dòng, ví dụ:
  int foo, fooarray[]; //WRONG!

-	Initialization
Hãy cố gắng khởi tạo các biến cục bộ nơi chúng được khai báo. Bạn có thể tìm đọc thêm biến cục bộ, biến toàn cục và scope - phạm vi biến. (Link: https://gochocit.com/ky-thuat-lap-trinh/bien-toan-cuc-va-bien-cuc-bo-trong-chuong-trinh)

-	Placement
Bạn cần khởi tạo biến ngay sau khi bắt đầu “blocks”. 
Đừng đợi đến khi biến được sử dụng lần đầu tiên mới khởi tạo, điều này dẫn đến nhầm lẫn cho dev

void myMethod() {
    int int1 = 0;         // beginning of method block

    if (condition) {
        int int2 = 0;     // beginning of "if" block
        ...
    }
}

Ngoài ra bạn không nên tạo tên biến giống nhau trong cùng một “block”.

int count;
...
myMethod() {
    if (condition) {
        int count = 0;     // AVOID!
        ...
    }
    ...
}

## Statements
-	Simple Statements: Mỗi dòng nên chỉ chứa một statement.

argv++;         // Correct
argc--;         // Correct  
argv++; argc--; // AVOID!

-	return Statements: Mỗi “return” Statement với một giá trị không nên sử dụng dấu ngoặc đơn () ngoại trừ điều ấy làm chúng được rõ ràng hơn theo một cách nào đó (ví dụ toán tử 3 ngôi). 

return;return myDisk.size();
return (size ? size : defaultSize);

## White space
-	Blank lines: Các dòng trống cải thiện khả năng đọc bằng cách đặt các phần mã có liên quan về mặt logic.
Hai blank lines nên được sử dụng trong những trường hợp dưới đây:
  * Giữa các section của một file
  * Giữa class, function hoặc interface.
Một blank line nên được sử dụng trong những trường hợp dưới đây:
  * Giữa các methods
  * Giữa biến nội bộ trong một phương thức và statement đầu tiên. 
  * Trước một block hoặc single-line comment.
  * Giữa các phần logic trong một phương thức để cải thiện tính dễ hiểu.
- Blank spaces nên được sử dụng trong những trường hợp sau:
  * Một từ khóa nằm trong dấu ngoặc đơn được chỉ định cách một “space”.
  while (true) {
           ...
       }
Lưu ý: Không nên sử dụng “space” giữa tên phương thức và dấu ngoặc đơn. Điều này giúp phân biệt từ khóa với các tên của phương thức.

  * Tất cả toán tử ngoại trừ “.” Nên được phân tách khỏi toán hạng của chúng bằng “space”. 

  a += c + d;
  a = (a + b) / (c * d);
    
  while (d++ = s++) {
      n++;
  }
  printSize("size is " + foo + "\n");

  * Biểu thức trong “for” statement nên cách bởi “space”.
  for (expr1; expr2; expr3)

## Naming Conventions
Quy tắc đặt tên sẽ khiến chương trình của bạn dễ hiểu hơn bởi chúng sẽ dễ để đọc. Chúng còn có thể cung cấp thông tin về chức năng rõ ràng, ví dụ như các hằng số, class, method, biến và interface.

- Classes: Class names should be nouns, in mixed case with the first letter of each internal word capitalized. Try to keep your class names simple and descriptive. Use whole words-avoid acronyms and abbreviations (unless the abbreviation is much more widely used than the long form, such as URL or HTML).
Examples: class Raster; class ImageSprite;

- Interfaces: Interface names should be capitalized like class names.
Examples: interface RasterDelegate; interface Storing;

- Methods: Methods should be verbs, in mixed case with the first letter lowercase, with the first letter of each internal word capitalized.
Examples: run(); runFast(); getBackground();

- Variables: 
  * Except for variables, all instance, class, and class constants are in mixed case with a lowercase first letter. Internal words start with capital letters. Variable names should not start with underscore _ or dollar sign $ characters, even though both are allowed.
  * Variable names should be short yet meaningful. The choice of a variable name should be mnemonic- that is, designed to indicate to the casual observer the intent of its use. One-character variable names should be avoided except for temporary "throwaway" variables. Common names for temporary variables are i, j, k, m, and n for integers; c, d, and e for characters.
Examples: int i; char c; float myWidth;

- Constants: The names of variables declared class constants and of ANSI constants should be all uppercase with words separated by underscores ("_"). (ANSI constants should be avoided, for ease of debugging.)
Examples: static final int MIN_WIDTH = 4; static final int MAX_WIDTH = 999; static final int GET_THE_CPU = 1;

# End

# Tham khảo:
- Nếu bạn sử dụng Visual Studio Code bạn có thể cài đặt extension sonarlint hoặc eslint. Chúng sẽ cảnh báo nếu mã code của bạn chưa thật sự đúng "rule code convention".
- Bạn có thể vào link https://rules.sonarsource.com/ chọn language, ở đây sẽ là custom của coding convention theo language. 